package me.bnmlks.mongocreatelock.exception;

/**
 * Concurrent document modification document had un-tracked
 * changes applied in the database, update should be retried.
 */
public class OptimisticLockException extends RuntimeException { }
