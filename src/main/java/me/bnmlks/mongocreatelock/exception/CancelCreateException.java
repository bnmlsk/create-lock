package me.bnmlks.mongocreatelock.exception;

/**
 * Document to create has acquired lock but it
 * has been reacquired and used by another document
 * so creation has to be aborted and update should
 * be applied instead.
 */
public class CancelCreateException extends RuntimeException { }
