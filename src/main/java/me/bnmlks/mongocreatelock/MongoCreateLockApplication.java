package me.bnmlks.mongocreatelock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongoCreateLockApplication {

    public static void main(String[] args) {
        SpringApplication.run(MongoCreateLockApplication.class, args);
    }
}
