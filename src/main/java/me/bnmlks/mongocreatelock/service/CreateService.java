package me.bnmlks.mongocreatelock.service;

import com.mongodb.*;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.TransactionBody;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.result.InsertOneResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldNameConstants;
import lombok.extern.slf4j.Slf4j;
import me.bnmlks.mongocreatelock.exception.CancelCreateException;
import one.util.streamex.StreamEx;
import org.bson.Document;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

import static com.mongodb.client.model.Projections.elemMatch;
import static com.mongodb.client.model.ReturnDocument.AFTER;
import static com.mongodb.client.model.Updates.set;

@Slf4j
@Service
@RequiredArgsConstructor
public class CreateService {

    private final ModelUniqueKeysExtractor keysExtractor;
    private final MongoClient mongoClient;
    private final MongoCollection<Document> fooCollection;
    private final MongoCollection<Document> lockCollection;

    @Value("${mongo-create-lock.mongo.lock.timeout}")
    private Duration lockDuration;

    /**
     * Create new document with duplication prevention mechanism.
     *
     * @param newDocument documents to be saved
     * @return created document if succeeds
     * @throws CancelCreateException if another process it attempting or already created document with the same keys
     */
    public Document create(Document newDocument) {

        Document lock = lock(newDocument);
        if (lock == null) {
            throw new CancelCreateException();
        }

        // It is important to keep insert operation and lock finalisation as an atomic operation:
        // in case if non atomic "replacement" of next algorithm:
        // 1) insert document
        // 2) fail to finish lock
        // 3) delete inserted document
        // another operation that relies on non-duplicated records may happen between steps 2) and 3)
        try (final ClientSession clientSession = mongoClient.startSession()) {
            TransactionOptions txnOptions = TransactionOptions.builder()
                    .readPreference(ReadPreference.primary())
                    .readConcern(ReadConcern.MAJORITY)
                    .writeConcern(WriteConcern.MAJORITY)
                    .build();

            TransactionBody<ObjectId> transactionBody = () -> {

                newDocument.put("version", 1);
                InsertOneResult insertOneResult = fooCollection.insertOne(clientSession, newDocument);

                // mark lock as "created" so no other process could do create with the same keys and had to retry
                Document finishedLock = lockCollection.findOneAndUpdate(lock, set(CreateLock.Fields.created, true));
                if (finishedLock == null) {
                    log.info("Lock {} has expired before insert operation happened, rollback create of {}", lock, newDocument);
                    throw new CancelCreateException();
                }

                return insertOneResult.getInsertedId().asObjectId().getValue();
            };

            ObjectId createdDocumentId = clientSession.withTransaction(transactionBody, txnOptions);

            Document createdDocument = fooCollection.find(new Document("_id", createdDocumentId)).first();
            log.info("Created document {} with lock {}", createdDocument, lock);
            return createdDocument;
        }
    }

    @Data
    @FieldNameConstants
    @AllArgsConstructor
    private static class CreateLock {

        @BsonId
        private ObjectId id;

        // Unique id of a lock to identify a process that holds it
        // Normal "id" is an identifier for lock document, but we need
        // process id to figure out who exactly uses the lock -- is it
        // current process or someone else got it first
        private ObjectId lockId;

        // Prevent holding lock forever
        private Date expiresAt;

        // List of unique keys to hold creation lock on
        // Intersecting set of keys (by name and value) try
        // to create document with the same lock
        private List<Document> keys;

        // Flags whether create has been fired for this lock or not
        // To avoid:
        // 1. creating a document when another document
        // has been sent for create but got to obtain the lock AFTER lock for
        // another create has been just removed
        // 2. obtaining a lock when already stored lock has already been
        // used for creation
        private boolean created;
    }

    /**
     * Acquire lock of document keys
     *
     * @param document to block creation from other threads
     * @return lock document if acquired, null otherwise
     */
    private Document lock(Document document) {
        try {

            List<Document> keys = keysExtractor.extract(document);
            LocalDateTime expiresAt = LocalDateTime.now(ZoneId.of("UTC")).plus(lockDuration);

            log.info("Trying to obtain lock for keys {}", keys);

            Document createLock = new Document()
                    .append(CreateLock.Fields.lockId, new ObjectId()) // create new lockId for current operation
                    .append(CreateLock.Fields.expiresAt, Date.from(expiresAt.toInstant(ZoneOffset.UTC)))
                    .append(CreateLock.Fields.keys, keys)
                    .append(CreateLock.Fields.created, false);

            // generate list fo keys for a lock
            // lock should prevent create for any record
            // where any of this key matches -- hence
            // "or" operation is used
            Bson keysMatch = StreamEx.of(keys)
                    .map(key -> elemMatch(CreateLock.Fields.keys, key))
                    .toListAndThen(Filters::or);

            // find if lock for such keys exists
            Document foundLock = lockCollection.find(keysMatch).first();
            if (foundLock == null) {
                // nothing locks this, trying to acquire new lock by keys

                FindOneAndUpdateOptions findOneAndUpdateOptions = new FindOneAndUpdateOptions()
                        .upsert(true)
                        .returnDocument(AFTER);

                Bson nonCreatedLockFilter = Filters.and(new Document(CreateLock.Fields.created, false), keysMatch);
                Document newLock = lockCollection.findOneAndUpdate(nonCreatedLockFilter, new Document("$set", createLock), findOneAndUpdateOptions);
                if (newLock == null) {
                    log.info("Lock for {} has already been used to create another product, retry for update", keys);
                    return null;
                }
                log.info("No current lock found, locked keys {} creation with new lock {}", keys, newLock);
                return newLock;
            } else if (foundLock.getBoolean(CreateLock.Fields.created)) {
                log.info("Lock for {} has already been used to create another product, retry for update", keys);
                return null;
            } else if (foundLock.getDate(CreateLock.Fields.expiresAt).before(new Date())){
                // this lock has expired, trying to reacquire it with current lock
                // update "lockId" value to claim lock for current instance; find lock
                // to update by found lockId to avoid updating the element if there are
                // other thread that tries to acquire the same lock (it will have different
                // lock id and if it has update first, then it is going to do the create)

                Document filter = new Document()
                        .append(CreateLock.Fields.id, foundLock.getObjectId(CreateLock.Fields.id))
                        .append(CreateLock.Fields.lockId, foundLock.getObjectId(CreateLock.Fields.lockId))
                        .append(CreateLock.Fields.created, false);

                FindOneAndUpdateOptions findOneAndUpdateOptions = new FindOneAndUpdateOptions()
                        .upsert(true)
                        .returnDocument(AFTER); // if update succeeds (old lock successfully updated) new lock value is returned
                Document updatedLock = lockCollection
                        .withWriteConcern(WriteConcern.MAJORITY)
                        .findOneAndUpdate(filter, new Document("$set", createLock), findOneAndUpdateOptions);
                if (updatedLock == null) {
                    log.info("Found lock {} was re-acquired by another process, create cannot be locked for keys {}, abort operation; retry for update", foundLock, keys);
                    return null;
                }

                log.info("Reacquired lock {} by {}", foundLock, updatedLock);

                return updatedLock;
            } else {
                log.info("Create for keys {} is locked by {}", keys, foundLock.getObjectId(CreateLock.Fields.lockId));
                return null;
            }
        } catch (MongoCommandException exception) {
            if (exception.getErrorCode() == 11000) {
                log.info("Failed to create duplicated lock for keys of {}", document);
            } else {
                log.info("Failed to execute mongo command for lock acquisition for {}", document, exception);
            }
            return null;
        }
    }
}
