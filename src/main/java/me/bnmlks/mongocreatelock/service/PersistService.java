package me.bnmlks.mongocreatelock.service;

import com.mongodb.client.MongoCollection;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.bnmlks.mongocreatelock.exception.CancelCreateException;
import me.bnmlks.mongocreatelock.exception.OptimisticLockException;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.format;

@Slf4j
@Service
@AllArgsConstructor
public class PersistService {

    private final CreateService createService;
    private final UpdateService updateService;

    private final ModelUniqueKeysExtractor keysExtractor;
    private final MongoCollection<Document> fooCollection;

    /**
     * Persist document -- create or update determined on unique keys set.
     *
     * Retryable to handle creates cancellations (in case other process creates document
     * with the same keys) and optimistic locks (in case other process updates known state
     * and refetch should be performed).
     *
     * @param document record to be saved
     * @return saved document
     */
    @Retryable(maxAttempts = 100, include = {OptimisticLockException.class, CancelCreateException.class})
    public Document persist(Document document) {

        Bson matcher = keysExtractor.matcher(document);
        List<Document> found = fooCollection.find(matcher).into(newArrayList());
        if (found.isEmpty()) {
            return createService.create(document);
        } else if (found.size() == 1) {
            return updateService.update(document, found.get(0));
        } else {
            throw new IllegalStateException(format("Duplicates found for keys %s: %s", matcher, found));
        }
    }
}
