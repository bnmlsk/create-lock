package me.bnmlks.mongocreatelock.service;

import com.mongodb.client.model.Filters;
import one.util.streamex.EntryStream;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Service
public class ModelUniqueKeysExtractor {

    private static final List<String> FIELDS = newArrayList("foo", "bar");

    public Bson matcher(Document document) {
        return EntryStream
                .of(document)
                .filterKeys(FIELDS::contains)
                .mapKeyValue((name, value) -> (Bson) new Document(name, value))
                .toListAndThen(Filters::or);
    }

    public List<Document> extract(Document document) {
        return EntryStream.of(document)
                .filterKeys(FIELDS::contains)
                .mapKeyValue((name, value) -> new Document("name", name).append("value", value))
                .toList();
    }
}
