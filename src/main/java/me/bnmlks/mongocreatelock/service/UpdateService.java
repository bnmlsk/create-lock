package me.bnmlks.mongocreatelock.service;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.bnmlks.mongocreatelock.exception.OptimisticLockException;
import org.bson.Document;
import org.springframework.stereotype.Service;

import static com.mongodb.client.model.ReturnDocument.AFTER;

@Slf4j
@Service
@AllArgsConstructor
public class UpdateService {

    private static final String ID = "_id";
    private static final String VERSION = "version";

    private final MongoCollection<Document> fooCollection;

    public Document update(Document document, Document found) {

        log.info("Update found document {} with data {}", found, document);

        document.put(ID, found.getObjectId(ID));
        document.put(VERSION, found.getInteger(VERSION) + 1);

        Document filter = new Document(ID, found.getObjectId(ID)).append(VERSION, found.getInteger(VERSION));

        FindOneAndUpdateOptions findOneAndUpdateOptions = new FindOneAndUpdateOptions().returnDocument(AFTER);

        Document updated = fooCollection.findOneAndUpdate(filter, new Document("$set", document), findOneAndUpdateOptions);
        if (updated == null) {
            log.info("No document was updated by id and version, fetch new document version and retry");
            throw new OptimisticLockException();
        }
        return updated;
    }
}
