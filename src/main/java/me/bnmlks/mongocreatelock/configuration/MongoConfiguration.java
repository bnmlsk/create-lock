package me.bnmlks.mongocreatelock.configuration;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.google.common.collect.Sets.newHashSet;

@Configuration
public class MongoConfiguration {

    @Value("${mongo-create-lock.mongo.database}")
    private String database;

    @Bean
    public MongoCollection<Document> fooCollection(MongoClient mongoClient,
                                                   @Value("${mongo-create-lock.mongo.collection}") String collection) {
        return mongoClient.getDatabase(database).getCollection(collection);
    }

    @Bean
    public MongoCollection<Document> lockCollection(MongoClient mongoClient,
                                                    @Value("${mongo-create-lock.mongo.lock.collection}") String collection) {
        MongoDatabase mongoDatabase = mongoClient.getDatabase(database);
        if (!mongoDatabase.listCollectionNames().into(newHashSet()).contains(collection)) {
            // Pre-create collection
            // https://docs.mongodb.com/manual/reference/method/db.collection.findOneAndUpdate/#existing-collections-and-transactions
            mongoDatabase.createCollection(collection);

            // "To prevent the creation of multiple duplicate documents, create
            // a unique index on the name field. With the unique index in place,
            // then the multiple findAndModify commands will exhibit one of the
            // following behaviors"
            // https://docs.mongodb.com/manual/reference/command/findAndModify/index.html
            mongoDatabase.getCollection(collection)
                    .createIndex(new Document("keys.name", 1).append("keys.value", 1),
                                 new IndexOptions().unique(true));
        }
        return mongoDatabase.getCollection(collection);
    }
}
