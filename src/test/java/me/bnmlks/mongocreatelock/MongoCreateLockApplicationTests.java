package me.bnmlks.mongocreatelock;

import com.mongodb.client.MongoCollection;
import lombok.extern.slf4j.Slf4j;
import me.bnmlks.mongocreatelock.service.PersistService;
import one.util.streamex.IntStreamEx;
import org.bson.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;

import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
class MongoCreateLockApplicationTests {

    private static final ForkJoinPool FORK_JOIN_POOL = new ForkJoinPool(20);

    @Autowired
    private PersistService persistService;

    @Autowired
    private MongoCollection<Document> fooCollection;

    @Autowired
    private MongoCollection<Document> lockCollection;

    @BeforeEach
    void clear() {
        fooCollection.deleteMany(new Document());
        lockCollection.deleteMany(new Document());
    }

    @RepeatedTest(20)
    void should_not_create_duplicated_records_if_lock_acquired_by_any_of_field() {

        int amountToGenerate = 20;

        List<Document> saved = IntStreamEx
                .range(amountToGenerate)
                .mapToObj(index -> new Document("index", index)
                        .append("foo", "fooValue")
                        .append("bar", "barValue"))
                .parallel(FORK_JOIN_POOL)
                .map(persistService::persist)
                .toList();

        assertSavedDocuments(saved, amountToGenerate, 1);
    }

    @RepeatedTest(20)
    void should_not_create_duplicated_records_as_single_key_matches() {

        int amountToGenerate = 20;
        Random random = new Random();

        List<Document> saved = IntStreamEx
                .range(amountToGenerate)
                .mapToObj(index -> new Document("index", index)
                        .append("foo", "fooValue")
                        .append("bar", "barValue" + random.nextInt(10)))
                .parallel(FORK_JOIN_POOL)
                .map(persistService::persist)
                .toList();

        assertSavedDocuments(saved, amountToGenerate, 1);
    }

    @RepeatedTest(20)
    void should_not_create_duplicated_records_if_lock_and_create_two_distinct_documents() {

        int amountToGenerate = 20;
        Random random = new Random();
        List<Document> saved = IntStreamEx
                .range(amountToGenerate)
                .mapToObj(index -> {
                    int switcher = random.nextInt(2);
                    return new Document("index", index)
                            .append("foo", "fooValue" + switcher)
                            .append("bar", "barValue" + switcher);
                })
                .parallel(FORK_JOIN_POOL)
                .map(persistService::persist)
                .toList();

        assertSavedDocuments(saved, amountToGenerate, 2);
    }

    @RepeatedTest(10)
    void should_not_lock_creation_of_documents_which_keys_do_not_intersect() {

        // this is visible by logs -- no log re-acquisition happens

        int amountToGenerate = 20;

        List<Document> saved = IntStreamEx
                .range(amountToGenerate)
                .mapToObj(index -> {
                    return new Document("index", index)
                            .append("foo", "fooValue" + index)
                            .append("bar", "barValue" + index);
                })
                .parallel(FORK_JOIN_POOL)
                .map(persistService::persist)
                .toList();

        assertSavedDocuments(saved, amountToGenerate, amountToGenerate);
    }

    private void assertSavedDocuments(List<Document> saved, int amountToGenerate, int i) {
        log.info("Persisted {} elements {}", saved.size(), saved);

        List<Document> created = fooCollection.find().into(newArrayList());
        log.info("Overall elements in collection is {}:\n{}", created.size(), created);

        assertThat(saved).hasSize(amountToGenerate);
        assertThat(created).hasSize(i);
    }
}
