# Mongo document creation locks

To prevent creating duplicates on certain keys, Mongo provides `upsert` operation 
that can create a document atomically if no other with the same filtering key (usually
unique key) is found. 

In some cases it is not possible to figure out such a unique key -- e.g. document
has different keys that should be unique across whole collection but when created
they might not have a full set of them. In that case `upsert` operation cannot help.

This project implements flexible keys locking mechanism to prevent duplicated projects
to be created when they run in parallel processes.

## Algorithm
1. Use separate collection to store lock objects -- information about locked set of keys
for creation, what process blocks it, when lock expires and if creation has already happened.
2. When creating document check if there are any unexpired and not-created lock present -- if it does
abort creation and return (document should retry persist). Find executed on a list of keys -- check
if any of locks has any of keys from a new document.
3. If no lock found create new one using `findAndUpdate` operation -- in case no lock created 
document with the same keys (`created == false` check is in filter operation).
4. Create transaction to insert a new document and mark lock as `created = true`. In case lock 
update operation returns null (if another process acquired the same lock or if other process
has already finished creation completely) transaction get cancelled and no insert operation happens.
5. If transaction finishes successfully, it marks the creation of a document. 

There is no automatic lock removal from lock collection (to allow other processes to know that
another document with same keys was created, and it should be aborted). Another process to clear
this collection (e.g. remove all created locks that expire 1 day ago) should be placed.

# Pre-requisites

## Run Mongo replica set

It is essential to use Mongo replica set as the code uses transaction 
mechanism. Transactions run via client sessions that only available 
in replica sets. https://docs.mongodb.com/manual/core/transactions/

To start Mongo cluster from 3 replica sets run use `./mongo-replica-set/docker-compose.yml` 
file (kudos to this guide https://gist.github.com/harveyconnor/518e088bad23a273cae6ba7fc4643549)

```bash
cd mongo-replica-set
docker-compose up -d
```

### Indexes
It is important to create indexes on lock collection as to implement atomicity
of updates the code uses `findAndUpdate` operation. Check documentation for 
details https://docs.mongodb.com/manual/reference/command/findAndModify/index.html.

# Examples
Check `MongoCreateLockApplicationTests` for examples. Note: test should be executed
when Mongo replica set is up.